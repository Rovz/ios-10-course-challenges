//
//  ViewController.swift
//  Egg Timer
//
//  Created by Robin Nizov on 2016-11-04.
//  Copyright © 2016 Robin Nizov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // Vars
    @IBOutlet var timerText: UILabel!
    
    var timer = Timer()
    var counter = 210
    
    
    // Actions
    @IBAction func pressPause(_ sender: UIBarButtonItem) {
        timer.invalidate()
    }
    @IBAction func pressStart(_ sender: UIBarButtonItem) {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.decreaseTimer), userInfo: nil, repeats: true)
    }
    @IBAction func pressDecrease(_ sender: UIBarButtonItem) {
        if counter > 10 {
            counter -= 10
            updateText()
        }
    }
    @IBAction func pressIncrease(_ sender: UIBarButtonItem) {
        if counter < 9000 {
            counter += 10
            updateText()
        }
    }
    @IBAction func pressReset(_ sender: UIBarButtonItem) {
        counter = 210
        updateText()
    }
    
    // Funcs
    func decreaseTimer() {
        if counter > 1 {
            counter -= 1
            updateText()
        }   else {
            timer.invalidate()
            timerText.text = "Eggs are done!"
        }
    }
    
    func updateText() {
        timerText.text = String(counter)
    }

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

