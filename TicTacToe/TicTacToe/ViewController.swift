//
//  ViewController.swift
//  TicTacToe
//
//  Created by Robin Nizov on 2016-11-14.
//  Copyright © 2016 Robin Nizov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var winnerLabel: UILabel!
    @IBOutlet var playAgainButton: UIButton!
    
    var activeGame = true
    var activePlayer = 1                                     // 1 = Noughts, 2 = Crosses
    var drawCounter = 0
    var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]     // 0 - Empty, 1 - Noughts, 2 - Crosses
    let winningCombinations = [
        [0, 1, 2], [0, 3, 6],[0, 4, 8],                         // first column, rows
        [3, 4, 5], [1, 4, 7], [2, 4, 6],                        // second column, columns
        [6, 7, 8], [2, 5, 8]]                                      // third column, diagonals
    
    func resetGameParameters() {
        activeGame = true
        activePlayer = 1
        gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        drawCounter = 0
    }
    
    func hideEndState() {
        winnerLabel.isHidden = true
        playAgainButton.isHidden = true
        winnerLabel.center = CGPoint(x: winnerLabel.center.x - 500, y: winnerLabel.center.y)
        playAgainButton.center = CGPoint(x : playAgainButton.center.x - 500, y: playAgainButton.center.y)
    }
    
    func triggerEndState() {
        activeGame = false
        winnerLabel.isHidden = false
        playAgainButton.isHidden = false
        UIView.animate(withDuration: 1, animations: {
            self.winnerLabel.center = CGPoint(x: self.winnerLabel.center.x + 500, y: self.winnerLabel.center.y)
            self.playAgainButton.center = CGPoint(x: self.playAgainButton.center.x + 500, y: self.playAgainButton.center.y)
        })
    }
    
    @IBAction func playAgain(_ sender: UIButton) {
        resetGameParameters()
        hideEndState()
        
        // Clears table
        for i in 1..<10 {
            if let button = view.viewWithTag(i) as? UIButton {
                    button.setImage(nil, for: [])
            }
        }
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        let activePosition = sender.tag - 1
        
        // Determine whos turn it is
        if gameState[activePosition] == 0 && activeGame {
            
            gameState[activePosition] = activePlayer
            
            if activePlayer == 1 {
                sender.setImage(UIImage(named: "nought.png"), for: [])
                drawCounter += 1
                activePlayer = 2
                
            } else {
                sender.setImage(UIImage(named: "cross.png"), for: [])
                drawCounter += 1
                activePlayer = 1
            }
            
            // Check for win
            for combination in winningCombinations {
                if gameState[combination[0]] != 0 && gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]] && activeGame {
                
                    // We have a winner!
                    if gameState[combination[0]] == 1 {
                        winnerLabel.text = "Noughts has won!"
                    } else {
                        winnerLabel.text = "Crosses has won!"
                    }
                    triggerEndState()
                }
            }
            
            // Check for draw
            if drawCounter == 9 && activeGame {
                winnerLabel.text = "Oops. It's a draw!"
                triggerEndState()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        hideEndState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

